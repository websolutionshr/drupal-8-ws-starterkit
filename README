# Drupal 8 WS StarterKit

This project provides a starter kit for Drupal 8 projects developed by WEBSOLUTIONS |HR. It
is very closely based on the [Platform.sh Example Drupal 8 project](https://github.com/platformsh/platformsh-example-drupal8).

## Starting a new project

To start a new Drupal 8 project:

1. Clone the repository

2. Build the project using Composer, remember to ALWAYS USE COMPOSER FROM PROJECT ROOT NOT FROM /web FOLDER!!

```
   composer install
```

3. Copy settings.php, settings.local.php and services.yml

cp defaults/settings.php web/sites/default/settings.php &&
cp defaults/settings.local.php web/sites/default/settings.local.php &&
cp defaults/services.yml web/sites/default/services.yml

4. Edit settings.local.php and enter your database name and credentials.

5. Go to you site and run through install script (install.php) to finish installing the site.

6. Enable additional dev modules, remember to ALWAYS USE DRUSH FROM /web FOLDER!!
```
   cd web
   drush en admin_toolbar
   drush en devel
   drush en coffee
```
7. Optionally enable Bootstrap theme
```
   drush en bootstrap
```


## Managing a Drupal site built with Composer

See the [Drupal documentation](https://www.drupal.org/node/2404989)
for tips on how best to leverage Composer with Drupal 8.

## How does this starter kit differ from vanilla Drupal from Drupal.org?

1. The `vendor` directory (where non-Drupal code lives) and the `config` directory
   (used for syncing configuration from development to production) are outside
   the web root. This is a bit more secure as those files are now not web-accessible.

2. The `default.settings.php` and `example.settings.local.php` files are preffiled with dev configuration.
